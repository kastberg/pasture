from tkinter import *
from dummy import Dummy

time_between_updates = 1000  # i millisekunder

width = 10  # antal rutor
height = 20  # antal rutor

fonster = Tk()

fonster.configure(background='#00d05e')
fonster.minsize(width=300,height=600)
fonster.maxsize(width=300,height=600)

empty = PhotoImage(file= r"empty.gif")

for i in range(width):
    for j in range(height):
        etikett = Label(fonster, image=empty, borderwidth=0)
        etikett.grid(column=i, row=j)

list_of_objects = []

dummy = Dummy(fonster)
list_of_objects.append(dummy)


def update_all_objects():
    for object in list_of_objects:
        object.update()

    fonster.after(time_between_updates, update_all_objects)


fonster.after(time_between_updates, update_all_objects)

fonster.mainloop()
