from tkinter import *


class Dummy:

    def __init__(self, fonster):
        self.photo = PhotoImage(file= r"dummy.gif")
        self.label = Label(fonster, image=self.photo, borderwidth=0)
        self.posx = 3
        self.posy = 3

    def update(self):
        print("Dummy uppdaterar.")
        self.posx += 1
        self.label.grid(column=self.posx, row=self.posy)
