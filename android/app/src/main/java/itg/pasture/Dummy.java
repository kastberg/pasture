package itg.pasture;


import android.graphics.Point;
import android.graphics.drawable.Drawable;


public class Dummy implements Entity {

    private final int image = R.drawable.dummy;

    protected Point position;

    protected PastureActivity pasture;

    public Dummy(PastureActivity pasture) {
        this.pasture = pasture;
    }

    public Dummy(PastureActivity pasture, Point position) {
        this.pasture   = pasture;
        this.position  = position;
    }

    @Override
    public Point getPosition() {
        return position;
    }

    @Override
    public void setPosition(Point newPosition) {
        position = newPosition;
    }

    @Override
    public void tick() {
        setPosition(new Point((int)getPosition().x+1,(int)getPosition().y));
    }

    @Override
    public int getImage() {
        return image;
    }

    @Override
    public String type() {
        return null;
    }
}
