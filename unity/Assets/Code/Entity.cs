﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;






public interface Entity
{
    void tick();

    Vector2 getPosition();
    void setPosition(Vector2 v);

    Texture getTexture();
}