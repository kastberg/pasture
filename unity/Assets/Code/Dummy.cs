﻿using UnityEngine;

public class Dummy : Entity
{
    private Texture texture = Resources.Load<Texture>("Textures/unknown");
    protected Vector2 position = new Vector2(0, 0);

    public void setPosition(Vector2 v)
    {
        position = v;
    }

    public Vector2 getPosition()
    {
        return position;
    }
    public void tick()
    {
        position.x += 1;
    }

    public Texture getTexture()
    {
        return texture;
    }
}