package itg.pasture;

import android.graphics.Point;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.GridLayout;
import android.widget.ImageView;

import java.util.ArrayList;
import java.util.List;

public class PastureActivity extends AppCompatActivity {
    final static int COLUMNS = 11;
    final static int ROWS = 20;
    public List<Entity> entities = new ArrayList<Entity>();



    private Runnable update = new Runnable() {
        @Override
        public void run() {
            Log.e("pasture","running");
            GridLayout grid = (GridLayout) findViewById(R.id.grid);

            for (int x = 0; x < COLUMNS; x++) {
                for (int y = 0; y < ROWS; y++) {
                    ImageView image = (ImageView) grid.getChildAt(y * COLUMNS + x);
                    image.setImageResource(R.drawable.empty);
                }
            }

            for (Entity e : entities) {
                e.tick();
                ImageView image = (ImageView) grid.getChildAt(e.getPosition().y * COLUMNS
                        + e.getPosition().x);
                image.setImageResource(e.getImage());
            }
            new Handler(getMainLooper()).postDelayed(this, 1000);

        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pasture);

        GridLayout grid = (GridLayout)findViewById(R.id.grid);
        for(int i=0;i<ROWS*COLUMNS;i++)
        {
            ImageView image = new ImageView(this);
            image.setImageResource(R.drawable.empty);
            grid.addView(image);
        }


        Dummy d = new Dummy(this);
        d.setPosition(new Point(5,10));
        entities.add(d);

        new Handler(getMainLooper()).postDelayed(update, 1000);

    }
}
