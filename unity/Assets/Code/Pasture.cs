﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pasture : MonoBehaviour
{ 

	public int rows;
    public int columns;
    public GameObject EntityPrefab;

    private GameObject[,] grid;
    public List<Entity> entities;
    
	void Start()
	{
        entities = new List<Entity>();
        grid = new GameObject[rows, columns];
        transform.position = new Vector3(rows / 2.0f, 0f, columns / 2.0f);
        transform.localScale = new Vector3(rows / 10.0f, 1.0f, columns / 10.0f);

        for (int x = 0; x < rows; x++)
        {
            for (int y = 0; y < columns; y++)
            {
                Vector3 position = new Vector3(0.5f+x, 0.5f, 0.5f+y);
                grid[x,y] = Instantiate(EntityPrefab, position, Quaternion.identity);
                grid[x, y].GetComponent<Renderer>().enabled = false;
            }
        }

        // Skapa test-sak.
        Dummy test = new Dummy();
        test.setPosition(new Vector2(4, 1));
        entities.Add(test);
	}


    void FixedUpdate()
    {

        foreach (GameObject g in grid)
        {
            g.GetComponent<Renderer>().enabled = false;
        }

        foreach (Entity e in entities)
        {
            e.tick();
            int x = (int)e.getPosition().x;
            int y = (int)e.getPosition().y;
            grid[x,y].GetComponent<Renderer>().enabled = true;
            grid[x,y].GetComponent<Renderer>().material.mainTexture = e.getTexture();
        }
    }
}